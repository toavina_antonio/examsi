CREATE DATABASE examsi;
 create role admin superuser;
grant all privileges on database examsi to admin;
create user resto superuser password '1234';
grant admin to resto;	

-- table front
 --table plat
 -- commande
 --commande par table
 -- -- -- -- -- 
 -- paiement
 -- 
--table categorie plat
CREATE TABLE table_restaurant(
    idTable_restaurant SERIAL NOT NULL
);
ALTER TABLE table_restaurant ADD PRIMARY KEY(idTable_restaurant);

INSERT INTO table_restaurant VALUES(1);
INSERT INTO table_restaurant VALUES(2);
INSERT INTO table_restaurant VALUES(3);
INSERT INTO table_restaurant VALUES(4);
INSERT INTO table_restaurant VALUES(5);
INSERT INTO table_restaurant VALUES(6);
INSERT INTO table_restaurant VALUES(7);
INSERT INTO table_restaurant VALUES(8);
INSERT INTO table_restaurant VALUES(9);
INSERT INTO table_restaurant VALUES(10);
INSERT INTO table_restaurant VALUES(11);

CREATE TABLE Paiement(
    idPaiement SERIAL NOT NULL,
    idCommandeParTable SERIAL,
    datePaiement TIMESTAMP
); 
ALTER TABLE Paiement ADD PRIMARY KEY(idPaiement);

CREATE TABLE commande(
    idCommande SERIAL NOT NULL,
    idPlat SERIAL,
    nombrePlat INTEGER
);
ALTER TABLE commande ADD PRIMARY KEY(idCommande);

CREATE TABLE commandeParTable(
    idCommandeParTable SERIAL NOT NULL,
    idCommande SERIAL NOT NULL,
    idTable SERIAL NOT NULL
);
ALTER TABLE commandeParTable ADD PRIMARY KEY(idCommandeParTable);

CREATE table Plat(
    idPlat SERIAL NOT NULL,
    nom VARCHAR(50),
    nombre INTEGER, -- stock
    prix INTEGER
);
ALTER TABLE Plat ADD PRIMARY KEY(idPlat);
INSERT INTO Plat VALUES(1,'SOUPE CHINOISE',15,4000);
INSERT INTO Plat VALUES(2,'SOUPE VAN TAN MINE',15,5000);
INSERT INTO Plat VALUES(3,'SOUPE SPECIALE',15,6000);
INSERT INTO Plat VALUES(4,'SOUPE FRUITS DE MER',15,6000);
INSERT INTO Plat VALUES(5,'TSOK',15,5000);

INSERT INTO Plat VALUES(6,'RIZ CANTONNAIS TSA-TSIOU',15,4000);
INSERT INTO Plat VALUES(7,'RIZ CANTONNAIS POULET',15,5000);
INSERT INTO Plat VALUES(8,'RIZ CANTONNAIS SPECIAL',15,5000);
INSERT INTO Plat VALUES(9,'BOL RENVERSE',15,5000);

INSERT INTO Plat VALUES(10,'SALADE DE FRUITS',15,2000);
INSERT INTO Plat VALUES(11,'FLAN',15,1000);
INSERT INTO Plat VALUES(12,'TARTE AU FRUITS',15,2000);

INSERT INTO Plat VALUES(13,'CAFE',15,2500);
INSERT INTO Plat VALUES(14,'THE',15,2500);
INSERT INTO Plat VALUES(15,'MILK-SHAKE',15,2500);

ALTER TABLE Paiement ADD FOREIGN KEY(idCommandeParTable) REFERENCES commandeParTable(idCommandeParTable);
ALTER TABLE commandeParTable ADD FOREIGN KEY(idCommande) REFERENCES commande(idCommande);
ALTER TABLE commandeParTable ADD FOREIGN KEY(idTable) REFERENCES table_restaurant(idTable_restaurant);


CREATE TABLE categoriePlat(
    idCategoriePlat SERIAL NOT NULL,
    nom VARCHAR(50)
);
ALTER TABLE categoriePlat ADD PRIMARY KEY(idCategoriePlat);
INSERT INTO categoriePlat VALUES(1,'Riz');
INSERT INTO categoriePlat VALUES(2,'Soupes');
INSERT INTO categoriePlat VALUES(3,'Desserts');
INSERT INTO categoriePlat VALUES(4,'Boissons');

CREATE TABLE Plat_Categorie(
    idPlat SERIAL,
    idCategoriePlat SERIAL
);
ALTER TABLE Plat_Categorie ADD FOREIGN KEY(idPlat) REFERENCES Plat(idPlat);
ALTER TABLE Plat_Categorie ADD FOREIGN KEY(idCategoriePlat) REFERENCES categoriePlat(idCategoriePlat);

ALTER TABLE Paiement ADD FOREIGN KEY(idCommandeParTable) REFERENCES commandeParTable(idCommandeParTable);
ALTER TABLE commande ADD FOREIGN KEY(idPlat) REFERENCES Plat(idPlat);

ALTER TABLE commandeParTable ADD FOREIGN KEY(idCommande) REFERENCES commande(idCommande);
ALTER TABLE commandeParTable ADD FOREIGN KEY(idTable) REFERENCES table_restaurant(idTable_restaurant);





INSERT INTO Plat_Categorie values(6,1);
INSERT INTO Plat_Categorie values(7,1);
INSERT INTO Plat_Categorie values(8,1);
INSERT INTO Plat_Categorie values(9,1);


INSERT INTO Plat_Categorie values(1,2);
INSERT INTO Plat_Categorie values(2,2);
INSERT INTO Plat_Categorie values(3,2);
INSERT INTO Plat_Categorie values(4,2);
INSERT INTO Plat_Categorie values(5,2);

INSERT INTO Plat_Categorie values(10,3);
INSERT INTO Plat_Categorie values(11,3);
INSERT INTO Plat_Categorie values(12,3);

INSERT INTO Plat_Categorie values(13,4);
INSERT INTO Plat_Categorie values(14,4);
INSERT INTO Plat_Categorie values(15,4);

CREATE VIEW Plat_Details as SELECT Plat.*,categoriePlat.idCategoriePlat,categoriePlat.nom as nom_Categorie from Plat_Categorie 
join categoriePlat on categoriePlat.idCategoriePlat=Plat_Categorie.idCategoriePlat 
join Plat on Plat_Categorie.idPlat=Plat.idPlat;
--- Serveurs
    

--table back
CREATE TABLE Users(
    idUsers SERIAL NOT NULL,
    login VARCHAR(50),
    password VARCHAR(80),
    idProfiles SERIAL  -- type utilisateur exemple 
);
ALTER TABLE Users ADD PRIMARY KEY(idUsers);

CREATE TABLE Profiles(
    idProfiles SERIAL NOT NULL,
    nameProfiles VARCHAR(50)
);
ALTER TABLE Profiles ADD PRIMARY KEY(idProfiles); 

INSERT INTO Profiles VALUES(default,'admin');
INSERT INTO Profiles VALUES(default,'users');

ALTER TABLE Users ADD FOREIGN KEY(idProfiles) REFERENCES Profiles(idProfiles); 

INSERT INTO Users VALUES(default,'admin','1234',1);
INSERT INTO Users VALUES(default,'server','server',2);
-- INSERT INTO Users VALUES(default,);


CREATE TABLE FACTURE(
    idFacture VARCHAR(5),
    dateFacture TIMESTAMP
);

ALTER TABLE FACTURE ADD PRIMARY KEY(idFacture);