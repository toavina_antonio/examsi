<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tandem</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <script src="<?php echo base_url();?>assets/bootstrap/js/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>

<div class="jumbotron">
  <div class="container text-center">
    <h1>Kaly Milay</h1>      
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav">
            <li>
                <form action="" class="navbar-form">
                  <div class="input-group">
                      <input type="number" min="1" class="form-control " placeholder="Table number">
                    <div class="input-group-btn">
                      <button type="submit" class="btn btn-default">OK</button>
                    </div>
                    
                  </div>
                </form>
            </li>
            <li><a href="#">Plats</a></li>
        </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Votre compte</a></li>
        <li><a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-shopping-cart"></span> Commandes</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Commandes de la table</h4>
        </div>
        <div class="modal-body">
            <p>Atao ato lay liste commandes.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
        
    </div>
</div>
<div class="container" id="contenus">   
<div class="row" id="foods">
    <?php 
      foreach($table as $t){
        $num=0;
        foreach($t as $t){$num=$t;}
        ?>
        <div class="col-sm-4">
        <div class="panel panel-primary">
          <div class="panel-heading"></div>
          <div class="panel-body"><a href="#" onclick="voirPlats(<?php echo $num; ?>,'<?php echo base_url();?>')">Table: <?php echo $num; ?></a></div>
          <div class="panel-footer">
          </div>
        </div>
      </div>

    <?php  }
    ?>
   
  </div>
</div><br>


<footer class="container-fluid text-center">
        <p>Kaly Milay Copyright</p>  
        <p>033 12 345 67</p>  
        <p>kalymilay@gmail.com</p>  
</footer>
<script src="<?php echo base_url();?>assets/bootstrap/js/fonctions.js"></script>

</body>

<!-- Mirrored from www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_store&stacked=h by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 30 Sep 2017 14:29:09 GMT -->
</html>
