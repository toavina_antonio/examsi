<?php 
class categorieplat_model extends CI_Model{
    public $idCategoriePlat;
    public $nom;

    public function insert($data) { 
        if ($this->db->insert("categorieplat", $data)) { 
           return true; 
        } 
     } 
     public function select(){
        $query = $this->db->get('categorieplat');
        return $query->result_array();
     }
  
     public function delete($roll_no) { 
        if ($this->db->delete("categorieplat", "idcategorieplat = ".$roll_no)) { 
           return true; 
        } 
     } 
  
     public function update($data,$old_roll_no) { 
        $this->db->set($data); 
        $this->db->where("idcategorieplat", $old_roll_no); 
        $this->db->update("categorieplat", $data); 
     } 
  } 
  


?>